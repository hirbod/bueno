package com.bueno.server.controllers;

import com.bueno.server.dto.PingReq;
import com.bueno.server.dto.ServiceStatusResp;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

    @GetMapping("/ping")
    public ResponseEntity<ServiceStatusResp> getPing() {
        ServiceStatusResp resp = new ServiceStatusResp();
       return ResponseEntity.ok(resp);
    }

    @PostMapping("/ping")
    public ResponseEntity<Object> postPing(@RequestBody PingReq req) {
        System.out.println(req);
        return ResponseEntity.status(200).body(null);
    }


}
