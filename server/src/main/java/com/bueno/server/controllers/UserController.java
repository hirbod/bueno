package com.bueno.server.controllers;

import com.bueno.server.Model.UserModel;
import com.bueno.server.dto.CreateUserRequest;
import org.apache.catalina.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;

public class UserController {
    // create the hashmap
    private HashMap<String,User> storage;
    public UserController(){
        this.storage = new HashMap<>();
    }

    /*
    Q: what is the functionality of this API?
    A: create new user.

    Q: what information do you need to create a new user?
    A:uname,pass,name
    */
    @PostMapping("/user")
    public boolean createUser(@RequestBody CreateUserRequest request){

    }

    // write your get mapping here
    // Q: what is the functionality?
    // A:
    // Q: what information do you need to get the user and send it back to the screen?
    // A:
    // Q: what are the information we want to send back? do we need the username? do we need the password? do we need the name?


    // write your put mapping
    // Q: what is the functionality?
    // A:
    // Q: what information do you need to update the user?
    // A:
    // Q: what do you want to send back to the user after the update is done?
    // A:


    // write your delete mapping here
    // Q: what is the functionality?
    // A:
    // Q: what information do you need to delete the user?
    // A:
    // Q: what do you want to send back to the user after the delete is done?
    // A:

}


/*
    after all the functions are done, install postman and trigger your APIs
    https://apidog.com/articles/postman-send-json/

    create a bunch of user and see if you can retrieve them

    Next Step:
    1. after every update, write your hashmap to a text file
    2. on start load the text file into your hashmap as we've done previously
 */