package com.bueno.server.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserModel {
    private String username;
    private String password;
    private String name;
}
