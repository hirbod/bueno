package com.bueno.server.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@Data
public class PingReq {
    String firstname;
    Integer level;
}
