package com.bueno.server.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceStatusResp {
    String status;
    Long timestamp;

    public ServiceStatusResp() {
        this.status = "ok";
        this.timestamp = System.currentTimeMillis();
    }
}
